<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index');

    Route::prefix('pages')->group(function () {
    	Route::get('/', 'PageController@show');
    	Route::get('create', 'PageController@create');
    	Route::post('store', 'PageController@store');
    	Route::get('update', 'PageController@update');
    	Route::get('delete', 'PageController@delete');
    });
});

Route::prefix('shop')->group(function () {
    Route::get('/', 'ShopController@index');
    Route::get('/{category}', 'ShopController@category');
    Route::get('/{category}/{product}', 'ShopController@product');
});

Route::get('/account', 'UserController@account');
Route::get('/login', 'UserController@login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/register', 'UserController@register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/', 'PageController@index');
Route::get('/{slug}', 'PageController@view');