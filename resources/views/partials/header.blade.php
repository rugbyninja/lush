<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ $page['title'] }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">	
        <!-- Place favicon.ico in the root directory -->
	    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">	
		<!-- all CSS hear -->		
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="/css/nice-select.css">
        <link rel="stylesheet" href="/css/slick.min.css">
        <link rel="stylesheet" href="/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/css/mainmenu.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/responsive.css">	
        <script src="/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>

        <div class="wrapper home-2">
            <header>
                <!-- header-top start -->
                <div class="header-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-8">
                                <!-- support-area start -->
                                <div class="support-area">
                                    <div class="single-support">
                                        <div class="support-icon">
                                            <i class="fa fa-truck"></i>
                                        </div>
                                        <div class="support-description">
                                            <p>Free delivery guarantee</p>
                                        </div>
                                    </div>
                                    <div class="single-support">
                                        <div class="support-icon">
                                            <i class="fa fa-lock"></i>
                                        </div>
                                        <div class="support-description">
                                            <p>Secure Shopping</p>
                                        </div>
                                    </div>
                                    <div class="single-support">
                                        <div class="support-icon">
                                            <i class="fa fa-history"></i>
                                        </div>
                                        <div class="support-description">
                                            <p>14 Day Returns</p>
                                        </div>
                                    </div>
                                    <div class="single-support">
                                        <div class="support-icon">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        <div class="support-description">
                                            <p>Track Your Order</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- support-area end -->
                            </div>
                            <div class="col-lg-5 col-md-4">
                                <div class="top-right-wrapper">
                                    <div class="shopping-cart">
                                        <ul>
                                            <li><a href="#"><span class="shpping-cart-text">Your bag: Total £0.00</span><span class="item-total">0</span></a>
                                                <!-- shopping-cart-wrapper start -->
                                                <div class="shopping-cart-wrapper open-dropdown">
                                                    <ul>
                                                        <li>
                                                            <div class="shoping-cart-image">
                                                                <a href="single-product.html"><img src="/img/product/1.jpg" alt=""></a>
                                                                <span class="cart-sticker">1x</span>
                                                            </div>
                                                            <div class="shoping-product-details">
                                                                <h3>Autumn on the new middle-aged men's clothes hot V-neck long-sleeved sweater</h3>
                                                                <div class="cart-product-price">
                                                                    <span>£16.51</span>
                                                                </div>
                                                                <div class="product-size"><span><strong>Size</strong>: S</span></div>
                                                                <div class="product-color">
                                                                   <span><strong>Color</strong>: Yellow</span> 
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="shoping-cart-image">
                                                                <a href="single-product.html"><img src="/img/product/2.jpg" alt=""></a>
                                                                <span class="cart-sticker">1x</span>
                                                            </div>
                                                            <div class="shoping-product-details">
                                                                <h3>Contracted casual fashion Men sweater free shipping</h3>
                                                                <div class="cart-product-price">
                                                                    <span>£25.99</span>
                                                                </div>
                                                                <div class="product-size"><span><strong>Size</strong>: S</span></div>
                                                                <div class="product-color">
                                                                   <span><strong>Color</strong>: Yellow</span> 
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="cart-total">
                                                                <h5>Subtotal<span class="float-right">£39.79</span></h5>
                                                                <h5>Shipping<span class="float-right">£7.00</span></h5>
                                                                <h5>Taxes<span class="float-right">£39.79</span></h5>
                                                                <h5>Total<span class="float-right">£0.00</span></h5>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="shoping-checkout">
                                                        <a href="checkout.html" class="cart-btn">checkout</a>
                                                    </div>
                                                </div>
                                                <!-- shopping-cart-wrapper end -->
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- shopping-cart end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- header-top end -->
                <!-- header-mid-area start -->
                <div class="header-mid-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <!-- logo start -->
                                <div class="logo">
                                    <a href="index.html"><img src="/img/logo/logo.png" alt=""></a>
                                </div>
                                <!-- logo end -->
                            </div>
                            <div class="col-xs-12 col-md-8 col-lg-8">
                                <div class="mid-right-wrapper">
                                    <div class="user-info-top">
                                        <ul>
                                            @if(Auth::check())
                                            <li><a href="/account">Welcome, {{ Auth::user()->name }}</a></li>
                                            <li><a href="/logout">Logout</a></li>
                                            @else
                                            <li><a href="/register">Welcome, Guest</a></li>
                                            <li><a href="/login">Sign in</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="search-area">
                                        <form action="#">
                                            <input type="text" placeholder="Search our catalog">
                                            <button type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- header-mid-area end -->
                <!-- menu-arae start -->
                <div id="stickymenu" class="main-menu-area solidblockmenu">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-menu display-none">
                                    <nav>
                                         <ul>
                                            <li><a href="/">Home</a></li>
                                            </li>
                                            <li><a href="#">Shop<span> <i class="fa fa-angle-down"></i></span></a>
                                                <ul class="mega-menu">
                                                    <li><a href="/shop/aprons">Aprons</a>
                                                        <ul>
                                                            <li><a href="">Category 1</a></li>
                                                            <li><a href="">Category 2</a></li>
                                                            <li><a href="">Category 3</a></li>
                                                            <li><a href="">Category 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="/shop/bags">Bags</a>
                                                        <ul>
                                                            <li><a href="">Category 1</a></li>
                                                            <li><a href="">Category 2</a></li>
                                                            <li><a href="">Category 3</a></li>
                                                            <li><a href="">Category 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="/shop/cushions">Cushions</a>
                                                        <ul>
                                                            <li><a href="">Category 1</a></li>
                                                            <li><a href="">Category 2</a></li>
                                                            <li><a href="">Category 3</a></li>
                                                            <li><a href="">Category 4</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="/blog">blog</a></li>
                                            <li><a href="/contact">Contact Us</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Mobile Menu Area Start -->
                                <div class="mobile-menu-area hidden-md hidden-lg">
                                    <div class="mobile-menu">
                                        <nav id="mobile-menu-active">
                                            <ul>
                                                <li class="active"><a href="index.html">Home</a>
                                                    <ul>
                                                        <li><a href="index-2.html">Home Page 2</a></li>
                                                        <li><a href="index-3.html">Home Page 3</a></li>
                                                        <li><a href="index-4.html">Home Page 4</a></li>
                                                        <li><a href="index-5.html">Home Page 5</a></li>
                                                        <li><a href="index-6.html">Home Page 6</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">pages</a>
                                                    <ul>
                                                        <li><a href="#">Pages</a>
                                                            <ul>
                                                                <li><a href="about.html">About us</a></li>
                                                                <li><a href="frequently-question.html">FAQ</a></li>
                                                                <li><a href="my-account.html">my account</a></li>
                                                                <li><a href="error404.html">Error 404</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="blog.html">Blog</a>
                                                            <ul>
                                                                <li><a href="blog-fullwidth.html">None Sidebar </a></li>
                                                                <li><a href="blog-left.html">Sidebar Left </a></li>
                                                                <li><a href="blog.html">Sidebar Right </a></li>
                                                                <li><a href="blog-details.html">blog details</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#">Shop</a>
                                                            <ul>
                                                                <li><a href="shop-fullwidth.html">Full Width</a></li>
                                                                <li><a href="shop-right.html">Sidebar Right </a></li>
                                                                <li><a href="shop.html">List View </a></li>
                                                                <li><a href="shop.html">Trousers</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#">Baking Cups</a>
                                                            <ul>
                                                                <li><a href="cart.html">cart page</a></li>
                                                                <li><a href="wishlist.html">wishlist page</a></li>
                                                                <li><a href="checkout.html">checkout page</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li><a href="shop.html">Men</a>
                                                    <ul>
                                                        <li><a href="#">Blouse</a>
                                                            <ul>
                                                                <li><a href="shop-fullwidth.html">Aprons</a></li>
                                                                <li><a href="shop-right.html">Accessories</a></li>
                                                                <li><a href="shop.html">Cake Moulds</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="shop.html">Accessories</a>
                                                            <ul>
                                                                <li><a href="shop-fullwidth.html">Cake Moulds</a></li>
                                                                <li><a href="shop-right.html">Cake Pans</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="shop.html">Trousers</a>
                                                            <ul>
                                                                <li><a href="shop.html">Blouse</a></li>
                                                                <li><a href="shop.html">Bakeware</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li><a href="shop.html">women</a></li>
                                                <li><a href="shop.html">shop</a></li>
                                                <li><a href="blog.html">blog</a></li>
                                                <li><a href="contact.html">Contact Us</a></li>
                                            </ul>
                                        </nav>                          
                                    </div>
                                </div>
                                <!-- Mobile Menu Area End -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- menu-arae end -->
            </header>
<!--             <div class="slider-area">
                <div class="slider-active owl-carousel">
                    <div class="slider-wrapper">
                        <div class="single-slider">
                            <img src="img/slider/1.jpg" alt="">
                        </div>
                    </div>
                    <div class="slider-wrapper">
                         <div class="single-slider">
                            <img src="img/slider/2.jpg" height="637" width="100%" alt="">
                        </div>
                    </div>
                </div>
            </div> -->