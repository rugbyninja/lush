@extends('layout')

@section('content')

@if(isset($page['protected']))
@include($page['protected'])
@endif

@endsection