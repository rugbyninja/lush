            <!-- single-product-page-wrapper start -->
            <div class="single-product-page-wrapper">
                <!-- breadcrumb-area start  -->
                <div class="breadcrumb-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="breadcrumb-list">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active">single product</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- breadcrumb-area end  -->
                
                <!-- single-product-content start -->
                <div class="single-product-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-9 col-md-8 col-xs-12">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="single-product-tab">
                                            <div class="zoomWrapper">
                                                <div id="img-1" class="zoomWrapper single-zoom">
                                                    <a href="#">
                                                        <img id="zoom1" src="/img/product/larg-1.jpg" data-zoom-image="img/product/larg-1.jpg" alt="big-1">
                                                    </a>
                                                </div>
                                                <div class="single-zoom-thumb">
                                                    <ul class="s-tab-zoom single-product-active owl-carousel" id="gallery_01">
                                                        <li>
                                                            <a href="#" class="elevatezoom-gallery active" data-update="" data-image="img/product/larg-1.jpg" data-zoom-image="img/product/larg-1.jpg"><img src="/img/product/s1.jpg" alt="zo-th-1"/></a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/larg-2.jpg" data-zoom-image="img/product/larg-2.jpg"><img src="/img/product/s2.jpg" alt="zo-th-2"></a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/larg-3.jpg" data-zoom-image="img/product/larg-3.jpg"><img src="/img/product/s3.jpg" alt="ex-big-3" /></a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/larg-4.jpg" data-zoom-image="img/product/larg-4.jpg"><img src="/img/product/s4.jpg" alt="zo-th-4"></a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/larg-5.jpg" data-zoom-image="img/product/larg-5.jpg"><img src="/img/product/s5.jpg" alt="zo-th-5"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="single-product-info-area">
                                            <h2 class="product-name">Autumn on the new middle-aged men's clothes</h2>
                                            <div class="ratings">
                                               <i class="fa fa-star"></i>
                                               <i class="fa fa-star"></i>
                                               <i class="fa fa-star"></i>
                                               <i class="fa fa-star"></i>
                                               <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="product-desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                            </div>
                                            <div class="product-price-box">
                                               <span class="new-price">£123.00</span>
                                               <span class="old-price">£123.00</span>
                                            </div>
                                            <div class="quick-add-to-cart">
                                                <form class="modal-cart">
                                                    <div class="quantity">
                                                        <label>Quantity</label>
                                                        <div class="cart-plus-minus">
                                                            <input type="text" value="0" class="cart-plus-minus-box">
                                                        <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div><div class="inc qtybutton"><i class="fa fa-angle-up"></i></div></div>
                                                    </div>
                                                    <button type="submit" class="add-to-cart">Add to cart</button>
                                                </form>
                                           </div>
                                            <div class="product-meta">
                                                <span class="posted-meta-inner">
                                                    Categories: 
                                                    <a href="#">Accent Chests</a>,
                                                    <a href="#">Accent Tables</a>,
                                                    <a href="#">Furniture</a>,
                                                    <a href="#">Living room</a>
                                                </span>
                                            </div>
                                            <div class="single-product-sharing">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#" ><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="discription-tab">
                                            <!--discription-tab-menu start-->
                                            <div class="discription-tab-menu">
                                                <ul class="nav" role="tablist">
                                                    <li class="active"><a data-toggle="tab" href="#description">Description</a></li>
                                                    <li><a data-toggle="tab" href="#review">Reviews (1)</a></li>
                                                </ul>
                                            </div>
                                            <!--discription-tab-menu end-->
                                            <!--discription-tab-content start-->
                                            <div class="discription-tab-content">
                                                <div class="tab-content">
                                                    <div id="description" class="tab-pane fade in active">
                                                        <div class="description-content">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                                                            <p>Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et placerat vestibulum, metus nisi posuere nisl, in accumsan elit odio quis mi. Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget.</p>
                                                        </div>
                                                    </div>
                                                    <div id="review" class="tab-pane fade">
                                                        <div class="review-comment">
                                                            <h2 class="reviews-title">1 review for <span>Headphone ipsum</span></h2>
                                                            <ul>
                                                                <li>
                                                                    <div class="product-comment">
                                                                        <img alt="" src="/img/comment-author/author1.jpg">
                                                                        <div class="product-comment-content">
                                                                            <p><strong>admin</strong>
                                                                                -
                                                                                <span>July 19, 2018</span>
                                                                                <span class="product-comments-rating">
                                                                                    <i class="fa fa-star"></i>	
                                                                                    <i class="fa fa-star"></i>	
                                                                                    <i class="fa fa-star"></i>	
                                                                                    <i class="fa fa-star"></i>	
                                                                                    <i class="fa fa-star-o"></i>
                                                                                </span>
                                                                            </p>
                                                                            <div class="description">
                                                                                <p>roadthemes</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div class="review-form-wrapper">
                                                                <span class="comment-reply-title">Add a review </span>
                                                                <form class="review-form" action="#">
                                                                        <p class="comment-notes">
                                                                            <span id="email-notes">Your email address will not be published.</span>
                                                                            Required fields are marked
                                                                            <span class="required">*</span>
                                                                        </p>
                                                                        <div class="comment-form-rating">
                                                                            <p><label>Your rating</label></p>
                                                                            <div class="rating">
                                                                                <i class="fa fa-star-o"></i>
                                                                                <i class="fa fa-star-o"></i>
                                                                                <i class="fa fa-star-o"></i>
                                                                                <i class="fa fa-star-o"></i>
                                                                                <i class="fa fa-star-o"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-reviwer">
                                                                            <div class="comment-form-comment single-input-reviwer">
                                                                                <label>Your review <span class="required">*</span></label>
                                                                                <textarea rows="8" cols="40" name="message"></textarea>
                                                                            </div>
                                                                            <div class="review-comment-form-author single-input-reviwer">
                                                                                <label>Name <span class="required">*</span></label>
                                                                                <input type="text" required="required">
                                                                            </div>
                                                                            <div class="review-comment-form-email single-input-reviwer">
                                                                                <label>Email <span class="required">*</span></label>
                                                                                <input type="text" required="required">
                                                                            </div>
                                                                            <div class="comment-submit">
                                                                                <button class="form-button" type="submit">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--discription-tab-content end-->
                                        </div>
                                    </div>
                                </div>
                                <div class="related-products-area mt-40">
                                    <!-- product-area start -->
                                    <div class="product-area pb-30">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="section-titel">
                                                    <h2>You might also like</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-wrapper">
                                            <div class="row">
                                                <div class="product-active owl-carousel">
                                                    <div class="col-lg-3">
                                                        <!-- single-product start -->
                                                        <div class="single-product">
                                                            <div class="product-image">
                                                                <a href="single-product.html">
                                                                    <img src="/img/product/1.jpg" alt="">
                                                                </a>
                                                                <span class="sale-sticker">New</span>
                                                                <div class="product-action">
                                                                    <a href="#" class="quick-view" data-toggle="modal" data-target="#productModal">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="single-product-info">
                                                                <h3><a href="single-product.html">2018 men's autumn winter fashion thicker thermal printing hit color</a></h3>
                                                                <div class="product-price-box">
                                                                    <span class="old-price">£30.51</span>
                                                                    <span class="new-price">£25.51</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- single-product end -->
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <!-- single-product start -->
                                                        <div class="single-product">
                                                            <div class="product-image">
                                                                <a href="single-product.html">
                                                                    <img src="/img/product/11.jpg" alt="">
                                                                </a>
                                                                <span class="sale-sticker">New</span>
                                                                <div class="product-action">
                                                                    <a href="#" class="quick-view" data-toggle="modal" data-target="#productModal">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="single-product-info">
                                                                <h3><a href="single-product.html">2018 New arrival spring/autumn personality male cardigan </a></h3>
                                                                <div class="product-price-box">
                                                                    <span class="new-price">£50.99</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- single-product end -->
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <!-- single-product start -->
                                                        <div class="single-product">
                                                            <div class="product-image">
                                                                <a href="single-product.html">
                                                                    <img src="/img/product/10.jpg" alt="">
                                                                </a>
                                                                <span class="sale-sticker">New</span>
                                                                <div class="product-action">
                                                                    <a href="#" class="quick-view" data-toggle="modal" data-target="#productModal">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="single-product-info">
                                                                <h3><a href="single-product.html">The new autumn and winter 2018 men's round neck pullover sweater</a></h3>
                                                                <div class="product-price-box">
                                                                    <span class="new-price">£25.99</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- single-product end -->
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <!-- single-product start -->
                                                        <div class="single-product">
                                                            <div class="product-image">
                                                                <a href="single-product.html">
                                                                    <img src="/img/product/9.jpg" alt="">
                                                                </a>
                                                                <span class="sale-sticker">New</span>
                                                                <div class="product-action">
                                                                    <a href="#" class="quick-view" data-toggle="modal" data-target="#productModal">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="single-product-info">
                                                                <h3><a href="single-product.html">Male Dress Slim Fit Sweater For Men Plus XXL</a></h3>
                                                                <div class="product-price-box">
                                                                     <span class="old-price">£26.30</span>
                                                                    <span class="discount">-10%</span>
                                                                    <span class="new-price">£24.03</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- single-product end -->
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <!-- single-product start -->
                                                        <div class="single-product">
                                                            <div class="product-image">
                                                                <a href="single-product.html">
                                                                    <img src="/img/product/6.jpg" alt="">
                                                                </a>
                                                                <span class="sale-sticker">New</span>
                                                                <div class="product-action">
                                                                    <a href="#" class="quick-view" data-toggle="modal" data-target="#productModal">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="single-product-info">
                                                                <h3><a href="single-product.html">Hot 2018 long-sleeved sweater coat Slim thin section young men's solid color</a></h3>
                                                                <div class="product-price-box">
                                                                    <span class="new-price">£50.03</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- single-product end -->
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <!-- single-product start -->
                                                        <div class="single-product">
                                                            <div class="product-image">
                                                                <a href="single-product.html">
                                                                    <img src="/img/product/4.jpg" alt="">
                                                                </a>
                                                                <span class="sale-sticker">New</span>
                                                                <div class="product-action">
                                                                    <a href="#" class="quick-view" data-toggle="modal" data-target="#productModal">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="single-product-info">
                                                                <h3><a href="single-product.html">Comfortable casual Slim O neck long-sleeved sweater</a></h3>
                                                                <div class="product-price-box">
                                                                    <span class="old-price">£10.09</span>
                                                                    <span class="new-price">£20.09</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- single-product end -->
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <!-- single-product start -->
                                                        <div class="single-product">
                                                            <div class="product-image">
                                                                <a href="single-product.html">
                                                                    <img src="/img/product/13.jpg" alt="">
                                                                </a>
                                                                <span class="sale-sticker">New</span>
                                                                <div class="product-action">
                                                                    <a href="#" class="quick-view" data-toggle="modal" data-target="#productModal">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="single-product-info">
                                                                <h3><a href="single-product.html">2018 Mens Turtleneck Sweaters Solid green</a></h3>
                                                                <div class="product-price-box">
                                                                    <span class="new-price">£16.09</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- single-product end -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- product-area end -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-12">
                                <!-- one-sale-area start -->
                                <div class="one-sale-area">
                                    <div class="secton-titel-three">
                                        <h3> One sale</h3>
                                    </div>
                                    <div class="category-item-active owl-carousel">
                                        <div class="category-item-inner">
                                            <!-- category-item start -->
                                            <div class="category-item">
                                                <div class="product-img">
                                                    <a href="#"><img src="/img/product/s1.jpg" alt=""></a>
                                                </div>
                                                <div class="single-product-info">
                                                    <h3><a href="shop.html">Autumn on the new middle-aged men's clothes hot V-neck long-sleeved sweater</a></h3>
                                                    <div class="product-price-box">
                                                        <span class="new-price">£50.99</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- category-item end -->
                                            <!-- category-item start -->
                                            <div class="category-item">
                                                <div class="product-img">
                                                    <a href="#"><img src="/img/product/s2.jpg" alt=""></a>
                                                </div>
                                                <div class="single-product-info">
                                                    <h3><a href="shop.html">Comfortable casual Slim O neck long-sleeved sweater</a></h3>
                                                    <div class="product-price-box">
                                                        <span class="old-price">£20.50</span>
                                                        <span class="new-price">£10.99</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- category-item end -->
                                            <!-- category-item start -->
                                            <div class="category-item">
                                                <div class="product-img">
                                                    <a href="#"><img src="/img/product/s3.jpg" alt=""></a>
                                                </div>
                                                <div class="single-product-info">
                                                    <h3><a href="shop.html">2018 Mens Turtleneck Sweaters Solid green Men's Jumpers Knitted Pullover </a></h3>
                                                    <div class="product-price-box">
                                                        <span class="old-price">£26.30</span>
                                                        <span class="discount">-10%</span>
                                                        <span class="new-price">£24.03</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- category-item end -->
                                        </div>
                                        <div class="category-item-inner">
                                            <!-- category-item start -->
                                            <div class="category-item">
                                                <div class="product-img">
                                                    <a href="#"><img src="/img/product/s5.jpg" alt=""></a>
                                                </div>
                                                <div class="single-product-info">
                                                    <h3><a href="shop.html">Autumn on the new middle-aged men's clothes hot V-neck long-sleeved sweater</a></h3>
                                                    <div class="product-price-box">
                                                        <span class="new-price">£50.99</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- category-item end -->
                                            <!-- category-item start -->
                                            <div class="category-item">
                                                <div class="product-img">
                                                    <a href="#"><img src="/img/product/s6.jpg" alt=""></a>
                                                </div>
                                                <div class="single-product-info">
                                                    <h3><a href="shop.html">Comfortable casual Slim O neck long-sleeved sweater</a></h3>
                                                    <div class="product-price-box">
                                                        <span class="old-price">£20.50</span>
                                                        <span class="new-price">£10.99</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- category-item end -->
                                        </div>
                                    </div>
                                    <!-- one-sale-area end -->
                                    <div class="single-categories-1 tag-area mt-30">
                                        <div class="secton-titel-three">
                                            <h3>Tags</h3>
                                        </div>
                                        <div class="tagcloud mt-30">
                                            <a href="#">asian</a>
                                            <a href="#">brown</a>
                                            <a href="#">euro</a>
                                            <a href="#">fashion</a>
                                            <a href="#">france</a>
                                            <a href="#">hat</a>
                                            <a href="#">t-shirt</a>
                                            <a href="#">teen</a>
                                            <a href="#">travel</a>
                                            <a href="#">white</a>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single-product-content end -->
            </div>
                
            </div>
            <!-- single-product-page-wrapper end -->
            