@extends('admin.layout')

@section('view')

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<h2>Create New Page</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<h3>Hints & Tips</h3>
			<ul>
				<li>Tip 1</li>
				<li>Tip 2</li>
				<li>Tip 3</li>
			</ul>
		</div>
		<div class="col-md-10">
			<h3>Page Settings & Content</h3>
		<form action="/admin/pages/store" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-group">
				<label>Page Title <span class="required">*</span></label>
				<input type="text" name="title" class="form-control" required>
			</div>
			<div class="form-group">
				<label>Banner Type <span class="required">*</span></label>
				<select name="banner" class="form-control" required>
					<option value="" selected disabled>Please choose a banner type</option>
					<option value="carousel">Carousel</option>
					<option value="no-banner">No Banner</option>
				</select>
			</div>
			<div class="form-group">
				<label>Banner Image</label>
				<input type="file" name="images">
			</div>
			<div class="form-group">
				<label>SEO Title</label>
				<input type="text" name="seo_title" class="form-control" required>
			</div>
			<div class="form-group">
				<label>SEO Description</label>
				<textarea name="seo_description" class="form-control" required></textarea>
			</div>
			<div class="form-group">
				<label>Parent</label>
				<select name="parent" class="form-control">
					<option value="" selected>No Parent</option>
					@if(isset($pages[0]))
					@foreach($pages AS $page)
					<option value="{{ $page->id }}">{{ $page->title }}</option>
					@endforeach
					@endif
				</select>
			</div>
			<div class="form-group">
				<label>Custom HTML Content</label>
				<textarea name="content" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<input type="checkbox" name="auth" value="1">
				<label>Restrict to logged in users only?</label>
			</div>
			<div class="form-group">
				<input type="checkbox" name="menu" value="1">
				<label>Display in navigation?</label>
			</div>
			<div class="form-group">
				<input type="checkbox" name="active" value="1" checked>
				<label>Make page active?</label>
			</div>
			<button type="submit" class="btn btn-default">Create Page</button>
		</form>
		</div>
</div>
@endsection