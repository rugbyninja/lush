@extends('admin.layout')

@section('view')

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<h2>Pages</h2>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
			<h3 class="box-title">Responsive Hover Table</h3>

			<div class="box-tools">
			<div class="input-group input-group-sm" style="width: 150px;">
			<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

			<div class="input-group-btn">
			<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
			</div>
			</div>
			</div>
			</div>
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody>
						<tr>
						<th>Title</th>
						<th>Link</th>
						<th>Active</th>
						<th>Actions</th>
						</tr>
						@foreach($page['pages'] AS $page)
						<tr>
						<td>{{ $page->title }}</td>
						<td><a href="/{{ $page->slug }}" target=_blank">/{{ $page->slug }}</a></td>
						<td><span class="label label-success">Active</span></td>
						<td><a href="" class="btn btn-primary">Edit</a>
							<a href="" class="btn btn-danger">Delete</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection