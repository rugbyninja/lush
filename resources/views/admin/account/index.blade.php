@extends('charlie.layout')

@section('view')

        {{ $charlie['model']->links() }}
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
            	<h3 class="box-title">CMS Users</h3>
	            <div class="box-tools">
	           		<div class="input-group input-group-sm" style="width: 150px;">
						<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
	             		<div class="input-group-btn">
	                		<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
	              		</div>
	            	</div>
	          	</div>
        	</div>
            <div class="box-body table-responsive no-padding">
              	<table class="table table-hover">
                	<tbody>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Permissions</th>
							<th>Active</th>
							<th>Actions</th>
						</tr>
						@foreach($charlie['model'] AS $user)
						<tr>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ ucwords($user->permissions) }}</td>
							<td>
								@if($user->active==1)
								<span class="label label-success">Active</span>
								@else
								<span class="label label-danger">Inactive</span>
								@endif
							</td>
							<td>
             				 	<a href="{{ route('account-update', $user->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
             				 	<a class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
							</td>
						</tr>
						@endforeach
              		</tbody>
          		</table>
        	</div>
        </div>
    </div>
</div>

@endsection