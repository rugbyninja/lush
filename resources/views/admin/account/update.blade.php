@extends('charlie.layout')

@section('view')
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<h3>Guidance</h3>
			<ul>
				<li>Email address must be unique</li>
				<li>Password must be at least 8 characters long</li>
			</ul>
		</div>
		
		<!-- Update Name -->
		<div class="col-md-9">
			<h3>Update {{ $charlie['instance']->name }}</h3>
			<form action="{{ route('account-store') }}" method="post">
			{{ csrf_field() }}
			<h4>Account Information (Required)</h4>
			<div class="form-group">
				<label for="name">Full Name:</label>
				<input type="text" name="name" class="form-control" value="{{ $charlie['instance']->name }}" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" class="form-control" value="{{ $charlie['instance']->email }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group">
				<label for="active">Set as active?</label>
				<input type="checkbox" name="active" value="1"
				@if( $charlie['instance']->active==1)
				checked
				@endif
				>
			</div>

			<div class="form-group">
				<label for="permissions">Permissions Level:</label>
				<select name="permissions" class="form-control" required>
					<option value="" disabled>Please choose a permissions level</option>
					<option @if( $charlie['instance']->permissions=="admin" )
						selected
						@endif value="admin">Admin</option>
					<option 
					@if( $charlie['instance']->permissions=="admin" )
					selected
					@endif value="content">Content</option>
					<option 
					@if( $charlie['instance']->permissions=="admin" )
					selected
					@endif value="read">Read Only</option>
				</select>
			</div>

			<h4>Password Update (Optional)</h4>

			<div class="form-group">
				<label for="password">Update Password:</label>
				<input type="password" name="password" class="form-control">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group">
				<label for="password_confirmation">Confirm Password:</label>
				<input type="password" name="password_confirmation" class="form-control">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group">
				<input class="btn btn-primary" type="submit" name="submit" value="Update">
			</div>

			</form>
		</div>
	</div>
</div>
@endsection