@@extends('charlie.layout')

@@section('view')
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<h3>Guidance</h3>
			<ul>
				
@foreach($columns AS $column)
	@if($column->Null=="NO")
	<li>{{ $column->Field }} is required</li>
	@endif
@endforeach

			</ul>
		</div>
		
		<div class="col-md-9">
			<h3>Create New CMS User</h3>
			<form action="{{ route('account-store') }}" method="post">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="name">Full Name:</label>
				<input type="text" name="name" class="form-control" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" class="form-control" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group">
				<label for="password_confirmation">Confirm Password:</label>
				<input type="password" name="password_confirmation" class="form-control" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group">
				<label for="active">Set as active?</label>
				<input type="checkbox" name="active" value="1">
			</div>

			<div class="form-group">
				<label for="permissions">Permissions Level:</label>
				<select name="permissions" class="form-control" required>
					<option value="" selected disabled>Please choose a permissions level</option>
					<option value="admin">Admin</option>
					<option value="content">Content</option>
					<option value="read">Read Only</option>
				</select>
			</div>

			<div class="form-group">
				<input class="btn btn-primary" type="submit" name="submit" value="Create">
			</div>

			</form>
		</div>
	</div>
</div>
@@endsection