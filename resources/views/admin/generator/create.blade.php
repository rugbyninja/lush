@@extends('charlie.layout')

@@section('view')
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<h3>Guidance</h3>
			<ul>
				
@foreach($columns AS $column)
	@if($column->Null=="NO")
	<li>{{ ucwords(str_replace('_', ' ',$column->Field)) }} is required</li>
	@endif
@endforeach

			</ul>
		</div>
		
		<div class="col-md-9">
			<h3>Create New {{ str_singular(ucwords(str_replace('_', ' ',$model))) }}</h3>
			<form action="/admin/{{ $model }}/store" method="post">
			@{{ csrf_field() }}

@foreach($columns AS $column)
	@if(! $column->Extra=="auto_increment")
			<div class="form-group">
				<label for="name">{{ ucwords(str_replace('_', ' ',$column->Field)) }}</label>
				<input type="text" name="{{ $column->Field }}" class="form-control"@if($column->Null=="NO" && $column->Default==null)
				required
				@endif
				>
                @@if ($errors->has('{{ $column->Field }}'))
                    <span class="help-block">
                        <strong>@{{ $errors->first('{{ $column->Field }}') }}</strong>
                    </span>
                @@endif
			</div>
	@endif
@endforeach


			<div class="form-group">
				<input class="btn btn-primary" type="submit" name="submit" value="Create">
			</div>

			</form>
		</div>
	</div>
</div>
@@endsection