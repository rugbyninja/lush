@extends('adminlte::page')

@section('title', env('APP_NAME').' :: CMS')

@section('content_header')

@stop

@section('content')
    @yield('view')
@stop