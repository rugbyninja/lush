
            <!-- login-register-page-wrapper start -->
            <div class="login-register-page-wrapper">
                <!-- breadcrumb-area start  -->
                <div class="breadcrumb-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="breadcrumb-list">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active">Login - Register</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- breadcrumb-area end  -->
                
                <!-- login-register-content start -->
                <div class="login-register-content mt-50">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="customer-login-register">
                                    <h3>Login</h3>
                                    <div class="login-Register-info">
                                        <form action="/login" method="post">
                                        {{ csrf_field() }} 
                                            <p class="coupon-input form-row-first">
                                                <label>Email <span class="required">*</span></label>
                                                <input type="text" name="email" required>
                                            </p>
                                            <p class="coupon-input form-row-last">
                                                <label>password <span class="required">*</span></label>
                                                <input type="password" name="password" required>
                                            </p>
                                           <div class="clear"></div>
                                            <p>
                                                <button value="Login" name="login" class="button-login" type="submit">Login</button>
                                                <label><input type="checkbox" value="1"><span>Remember me</span></label>
                                                <a href="#" class="password/reset">Lost your password?</a>
                                            </p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6  col-md-6 col-sm-12">
                                <div class="customer-login-register">
                                    <h3>Register</h3>
                                    <div class="login-Register-info">
                                        Click <a href="/register">here</a> to register a new account.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- shop-page-main-content end -->
            </div>
            <!-- login-register-page-wrapper end -->
            