
            <!-- login-register-page-wrapper start -->
            <div class="login-register-page-wrapper">
                <!-- breadcrumb-area start  -->
                <div class="breadcrumb-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="breadcrumb-list">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active">Login - Register</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- breadcrumb-area end  -->
                                   
                @if(count($errors))
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 alert alert-danger">
                            <h3>Form Errors</h3>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
                <!-- login-register-content start -->
                <form action="/register" method="post">
                {{ csrf_field() }}
                <div class="login-register-content mt-50">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6  col-md-6 col-sm-12">
                                <div class="customer-login-register">
                                    <h3>Your Details</h3>
                                    <div class="login-Register-info">
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <label>Full Name <span class="required">*</span></label>
                                            <input type="text" name="name" class="form-control" required>
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <label>Email <span class="required">*</span></label>
                                            <input type="email" name="email" class="form-control" required>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6  col-md-6 col-sm-12">
                                <div class="customer-login-register">
                                    <h3>Password</h3>
                                    <div class="login-Register-info">
                                            <p class="coupon-input form-row-first">
                                                <label>Password <span class="required">*</span></label>
                                                <input type="password" name="password" required>
                                            </p>
                                            <p class="coupon-input form-row-last">
                                                <label>Confirm Password <span class="required">*</span></label>
                                                <input type="password" name="password_confirmation" required>
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6  col-md-6 col-sm-12">
                                <div class="customer-login-register">
                                    <h3>Address</h3>
                                    <div class="login-Register-info">
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <label>1st Line of Address <span class="required">*</span></label>
                                            <input type="text" name="first_address" class="form-control" required>
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <label>2nd Line of Address</label>
                                            <input type="second_address" name="second_address" class="form-control">
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <label>Town / City <span class="required">*</span></label>
                                            <input type="text" name="town" class="form-control" required>
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <label>County <span class="required">*</span></label>
                                            <input type="text" name="county" class="form-control" required>
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <label>Postcode <span class="required">*</span></label>
                                            <input type="text" name="postcode" class="form-control" required>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6  col-md-6 col-sm-12">
                                <div class="customer-login-register">
                                    <h3>Terms & Conditions</h3>
                                    <div class="login-Register-info">
                                        <div class="form-group">
                                            <input type="checkbox" name="terms" required>
                                            Please confirm you have read and accept our <a href="/privacy">privacy policy</a> and <a href="/terms">terms and conditions</a> <span class="required">*</span>
                                        </div>
                                        <div class="form-group">
                                            <input type="checkbox" name="terms">
                                            Are you happy for us to contact you from time to time with news and special offers? We will never pass your details onto a third party and you can opt out at any time.
                                        </div>
                                        <div class="form-group">
                                            <p class="coupon-input form-row-first">
                                            <button class="btn btn-danger" type="submit">Create Account</button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <!-- shop-page-main-content end -->
            </div>
            <!-- login-register-page-wrapper end -->
            