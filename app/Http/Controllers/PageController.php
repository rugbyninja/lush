<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function view($slug)
    {
    	$page = [];
    	$page['title'] = $slug;

    	return view('view')->with('page', $page);
    }

    public function index()
    {

    	return $this->view('index');
    }

    public function show()
    {   
        $page = [];
        $page['pages'] = Page::all();
        return view('admin.crud.pages.index')->with('page', $page);
    }

    public function create()
    {
        $pages = Page::all();
        return view('admin.crud.pages.create')->with('pages', $pages);
    }

    public function update(Request $request)
    {
        return view('admin.crud.pages.update');
    }

    public function store(Request $request)
    {
        $create = $request->except('_token');
        $create['slug'] = str_slug($request->title);

        Page::create($create);
        return view('admin.crud.pages.index');
    }

    public function delete(Request $request)
    {
        return view('admin.crud.pages.index');
    }

}
