<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function account()
    {
    	if(! Auth::check()){
    		return redirect('/login');
    	}

    	$page = [];
    	$page['protected'] = 'users.account';
    	$page['title'] = env('APP_NAME').' '.Auth::user()->name.' Account';

    	return view('view')->with('page', $page);
    }

    public function login()
    {
    	if(Auth::check()){
    		return redirect('/account');
    	}

    	$page = [];
    	$page['protected'] = 'users.login';
    	$page['title'] = env('APP_NAME').' Login';

    	return view('view')->with('page', $page);
    }

    public function register()
    {
    	if(Auth::check()){
    		return redirect('/account');
    	}

    	$page = [];
    	$page['protected'] = 'users.register';
    	$page['title'] = env('APP_NAME').' Register';

    	return view('view')->with('page', $page);
    }
}
