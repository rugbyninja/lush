<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function index()
    {
    	$page = [];
    	$page['protected'] = 'shop.categories';
    	$page['title'] = env('APP_NAME').' Shop';

    	return view('view')->with('page', $page);
    }

    public function category($category)
    {
    	$page = [];
    	$page['protected'] = 'shop.category';
    	$page['title'] = env('APP_NAME').' '.$category;

    	return view('view')->with('page', $page);
    }

    public function product($category, $product)
    {
    	$page = [];
    	$page['protected'] = 'shop.product';
    	$page['title'] = env('APP_NAME').' '.$product;

    	return view('view')->with('page', $page);
    }

}
