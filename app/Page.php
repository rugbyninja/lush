<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
    	'title', 'slug', 'banner', 'images', 'seo_description', 'seo_title', 'parent', 'content', 'auth', 'menu', 'active',
   	];
}